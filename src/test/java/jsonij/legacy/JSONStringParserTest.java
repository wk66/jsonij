/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSONReader;
import cc.plural.jsonij.ReaderJSONReader;
import cc.plural.jsonij.StringJSONReader;
import cc.plural.jsonij.ThreadSafeJSONParser;
import cc.plural.jsonij.parser.ParserException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author jmarsden
 *
 */
public class JSONStringParserTest {

    ThreadSafeJSONParser parser;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        parser = new ThreadSafeJSONParser();
        parser.setLocale(Locale.ENGLISH);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test method for {@link com.realitypipe.json.JSONParser#parseString(com.realitypipe.json.JSONReader)}.
     * @throws IOException
     * @throws ParserException
     */
    @Test
    public void testParseString1() throws IOException, ParserException {
        String testString = "\"John\"";
        JSONReader target = new StringJSONReader(testString);
        JSON.String value = parser.parseString(target);
        if (!value.toString().equals("John")) {
            fail();
        }
    }

    /**
     * Test method for {@link com.realitypipe.json.JSONParser#parseString(com.realitypipe.json.JSONReader)}.
     * @throws IOException
     * @throws ParserException
     */
    @Test
    public void testParseString2() throws IOException, ParserException {
        String testString = "\"John\"\"Marsden\"";
        ByteArrayInputStream targetArrayStream = new ByteArrayInputStream(testString.getBytes());
        InputStreamReader targetReader = new InputStreamReader(targetArrayStream);
        JSONReader target = new ReaderJSONReader(targetReader);
        JSON.String value1 = parser.parseString(target);
        JSON.String value2 = parser.parseString(target);
        targetReader.close();
        targetArrayStream.close();
        assertEquals("John", value1.toString());
        assertEquals("Marsden", value2.toString());
    }

    @Test
    public void testParseString3() throws IOException, ParserException {
        String testString = "\"John\\tcool\"";
        ByteArrayInputStream targetArrayStream = new ByteArrayInputStream(testString.getBytes());
        InputStreamReader targetReader = new InputStreamReader(targetArrayStream);
        JSONReader target = new ReaderJSONReader(targetReader);
        JSON.String value = parser.parseString(target);
        targetReader.close();
        targetArrayStream.close();
        assertEquals("John\tcool", value.toString());
    }

    @Test
    public void testParseString4() throws IOException, ParserException {
        String testString = "\"Gah: \\u5b66\"";
        ByteArrayInputStream targetArrayStream = new ByteArrayInputStream(testString.getBytes());
        InputStreamReader targetReader = new InputStreamReader(targetArrayStream);
        JSONReader target = new ReaderJSONReader(targetReader);
        JSON.String value = parser.parseString(target);
        targetReader.close();
        targetArrayStream.close();
        assertEquals("Gah: \u5b66", value.toString());
    }
    
    @Test
    public void testParseString5() throws IOException, ParserException {
        String testString = "\"Gah: \\ud801\\udc01\"";
        ByteArrayInputStream targetArrayStream = new ByteArrayInputStream(testString.getBytes());
        InputStreamReader targetReader = new InputStreamReader(targetArrayStream);
        JSONReader target = new ReaderJSONReader(targetReader);
        JSON.String value = parser.parseString(target);
        targetReader.close();
        targetArrayStream.close();
        assertEquals("Gah: \ud801\udc01", value.toString());
    }

    @Test
    public void testStringDecodeEncode() throws IOException, ParserException {
        String jsonString = "\"John\\tMarsden\"";
        JSONReader target = new StringJSONReader(jsonString);
        JSON.String value = parser.parseString(target);
        assertEquals(value.toString(),"John\tMarsden");
        assertEquals(value.toJSON(),"\"John\\tMarsden\"");

        jsonString = "\"\\u5b66\"";
        target = new StringJSONReader(jsonString);
        value = parser.parseString(target);
        assertEquals(value.toString(),"\u5b66");
        assertEquals(value.toJSON(),"\"\\u5b66\"");

        jsonString = "\"\\ud801\\udc01\"";
        target = new StringJSONReader(jsonString);
        value = parser.parseString(target);
        assertEquals(value.toString(),"\ud801\udc01");
        assertEquals(value.toJSON(), "\"\\ud801\\udc01\"");
    }
}
