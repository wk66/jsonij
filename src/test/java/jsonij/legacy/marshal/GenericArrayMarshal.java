/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package jsonij.legacy.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import cc.plural.jsonij.marshal.JavaMarshaler;
import cc.plural.jsonij.marshal.annotation.JSONIgnore;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import jsonij.legacy.marshal.GenericArrayMarshal.RatedBook.QUALITY;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class GenericArrayMarshal {

    @Test
    public void testGenericArray() throws ParserException, IOException {
        System.out.println("testGenericArray");
        MyArray<Book<String>> bookList = new MyArray<Book<String>>();
        bookList.add(new RatedBook<String>("John Marsden", "Tomorrow When The War Began", "ISBN-0192-2928", QUALITY.GOOD));
        bookList.add(new RatedBook<String>("David Johnsonbaugh", "Discrete Mathematics", "ISBN-0192-2928", QUALITY.BAD));
        JavaMarshaler marshaler = new JavaMarshaler();
        Value output = marshaler.marshalObject(bookList);
        Value expected = JSON.parse("{\"$innerArray\":[{\"$innerArray\":[{\"$innerArray\":[],\"author\":\"John Marsden\",\"title\":\"Tomorrow When The War Began\",\"test\":-99,\"quality\":\"GOOD\"},{\"$innerArray\":[],\"author\":\"David Johnsonbaugh\",\"title\":\"Discrete Mathematics\",\"test\":-99,\"quality\":\"BAD\"},{\"$innerArray\":[\"A String\"],\"author\":\"R.A.Salvatore\",\"title\":\"Homeland\",\"iSBN\":\"ISBN-0192-2928\",\"test\":-99,\"quality\":\"GOOD\"}],\"value\":100}],\"someValue\":\"this is a string\"}\n").getRoot();
        System.out.println(output.toJSON());
        //assertEquals(output, expected);
    }

    @Test
    public void testNestedGenericArray() throws JSONMarshalerException {
        System.out.println("testNestedGenericArray");

        MyOuterArray<MyArray<Book<String>>> bookBookList = new MyOuterArray<MyArray<Book<String>>>();

        MyArray<Book<String>> bookList = new MyArray<Book<String>>();
        RatedBook<String> book = new RatedBook<String>("R.A.Salvatore", "Homeland", "ISBN-0192-2928", QUALITY.GOOD);
        book.add("A String");
        bookList.add(new RatedBook<String>("John Marsden", "Tomorrow When The War Began", QUALITY.GOOD));
        bookList.add(new RatedBook<String>("David Johnsonbaugh", "Discrete Mathematics", QUALITY.BAD));
        bookList.add(book);
        bookBookList.add(bookList);

        JavaMarshaler marshaler = new JavaMarshaler();
        Value output = marshaler.marshalObject(bookBookList);
        System.out.println(output.toJSON());
    }

    @Test
    public void testNestedGenericMap() throws JSONMarshalerException {
        MyMap1<java.lang.String, MyMap2<java.lang.String, Integer>> mapObject = new MyMap1<java.lang.String, MyMap2<java.lang.String, Integer>>();
        MyMap2<java.lang.String, Integer> innerMap = new MyMap2<java.lang.String, Integer>();
        innerMap.put("Some value", 22);
        innerMap.put("Another value", -3000);
        mapObject.put("innerMap1", innerMap);
        innerMap = new MyMap2<java.lang.String, Integer>();
        innerMap.put("blah", 69);
        mapObject.put("innerMap2", innerMap);

        JavaMarshaler marshaler = new JavaMarshaler();
        Value output = marshaler.marshalObject(mapObject);
        System.out.println(output.toJSON());
    }

    public class MyOuterArray<S extends MyArray<?>> extends ArrayList<S> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = 2050949577933010800L;
        String someValue;

        public MyOuterArray() {
            someValue = "this is a string";
        }

        public String getSomeValue() {
            return someValue;
        }

        public void setSomeValue(String someValue) {
            this.someValue = someValue;
        }
    }

    public class MyArray<S extends Book<?>> extends ArrayList<S> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = 8389046563607785747L;
        public int value = 100;
    }

    public class MyMap1<S, M extends Map<?, ?>> extends HashMap<S, M> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = -3727864143653102617L;
        public java.lang.String value = "rah";
    }

    public class MyMap2<S, I> extends HashMap<S, I> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = 6938490107578854400L;
    }

    public static class RatedBook<T> extends Book<T> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = -6664478337797979108L;
        public int test = -99;
        QUALITY quality;

        public enum QUALITY {

            GOOD, BAD
        }

        public RatedBook(String author, String title, QUALITY quality) {
            super(author, title);
            this.quality = quality;
        }

        public RatedBook(String author, String title, String isbn, QUALITY quality) {
            super(author, title, isbn);
            this.quality = quality;
        }

        public QUALITY getQuality() {
            return quality;
        }

        public void setQuality(QUALITY quality) {
            this.quality = quality;
        }
    }

    public static class Book<T> extends ArrayList<T> {

        /**
         * Serial UID
         */
        private static final long serialVersionUID = -366377652330081822L;
        String author;
        String title;
        String isbn;

        public Book() {
            this(null, null);
        }

        public Book(String author, String title) {
            this.author = author;
            this.title = title;
        }

        public Book(String author, String title, String isbn) {
            this.author = author;
            this.title = title;
            this.isbn = isbn;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @JSONIgnore
        public String getISBN() {
            return isbn;
        }

        @JSONIgnore
        public void setISBN(String isbn) {
            this.isbn = isbn;
        }
    }
}
