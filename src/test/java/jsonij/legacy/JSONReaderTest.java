/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package jsonij.legacy;

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.StringJSONReader;
import cc.plural.jsonij.parser.ParserException;
import cc.plural.jsonij.parser.ReaderParser;
import static org.junit.Assert.*;

/**
 *
 * @author jmarsden
 */
public class JSONReaderTest {

    public static final String TEST_STRING1;

    static {
        TEST_STRING1 = "  This    \t\t\r\n\t   is \t a  \t\r\n     string     \t  with   \t\t\r\n spaces.";
    }

    public JSONReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getStringReader method, of class JSONReader.
     */
    @Test
    public void testStringJSONReader() throws IOException, ParserException {
        System.out.println("getStringReader");
        StringJSONReader instance = new StringJSONReader(TEST_STRING1);
        char c;
        c = (char) instance.peek();
        assertEquals('T', c);
        c = (char) instance.read();
        assertEquals('T', c);
        c = (char) instance.peek();
        assertEquals('h', c);
        c = (char) instance.read();
        assertEquals('h', c);
        c = (char) instance.peek();
        assertEquals('i', c);
        c = (char) instance.read();
        assertEquals('i', c);
        c = (char) instance.peek();
        assertEquals('s', c);
        c = (char) instance.read();
        assertEquals('s', c);

        c = (char) instance.peek();
        assertEquals('i', c);
        c = (char) instance.read();
        assertEquals('i', c);

        c = (char) instance.read();
        assertEquals('s', c);
        
        ReaderParser stringReaderInstance = instance.getStringReader();
        c = (char) stringReaderInstance.peek();
        assertEquals(' ', c);
        c = (char) stringReaderInstance.read();
        assertEquals(' ', c);
        assertEquals('\t', stringReaderInstance.peek());
        assertEquals('\t', stringReaderInstance.read());
        assertEquals(' ', stringReaderInstance.read());
        assertEquals('a', stringReaderInstance.peek());
        assertEquals('a', stringReaderInstance.read());
        stringReaderInstance.close();
        assertEquals('s', instance.read());
        assertEquals('t', instance.read());
    }
}
