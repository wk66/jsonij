/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jvmserializers;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshaler;
import cc.plural.jsonij.parser.ParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MediaParseTest {

    public MediaParseTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMedia1InputStream() throws ParserException, IOException {
        System.out.println("testMedia1");
        for (int i = 0; i < 1; i++) {

            URL file = ClassLoader.class.getResource("/jvmtests/media.1.cks");

            JSON resultJSON = JSON.parse(file.openStream());

            String resultString = resultJSON.toString();
        }
    }

    @Test
    public void testMedia1String() throws ParserException, IOException, Exception {
        System.out.println("testMedia1String");

        StringBuilder result = new StringBuilder();

        try {
            InputStream inputStream = ClassLoader.class.getResourceAsStream("/jvmtests/media.1.cks");

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            char[] buf = new char[1024];

            int r = 0;

            while ((r = reader.read(buf)) != -1) {
                result.append(buf, 0, r);
            }
            reader.close();
        } finally {
        }

        String jsonString = result.toString();

        for (int i = 0; i < 10000; i++) {
            JSON mediaContentJsonObject = JSON.parse(jsonString);

            JSON.Object mediaJsonObject = (JSON.Object) mediaContentJsonObject.get("media");
            Media media = readMedia(mediaJsonObject);

            JSON.Array<Value> imageValues = (JSON.Array<Value>) mediaContentJsonObject.get("images");
            List<Image> images = readImages(imageValues);

            MediaContent mediaContent = new MediaContent();
            mediaContent.media = media;
            mediaContent.images = images;

            StringWriter writer = new StringWriter();
            writeMediaContent(writer, mediaContent);
            writer.flush();
            String output = writer.toString();
        }
    }

    static MediaContent readMediaContent(String mediaContentJsonInput) throws Exception {

        JSON.Object mediaContentJsonObject = (JSON.Object) JSON.parse(mediaContentJsonInput).getRoot();

        JSON.Object mediaJsonObject = (JSON.Object) mediaContentJsonObject.get("media");
        Media media = readMedia(mediaJsonObject);

        JSON.Array<Value> imageValues = (JSON.Array<Value>) mediaContentJsonObject.get("images");
        List<Image> images = readImages(imageValues);

        MediaContent mediaContent = new MediaContent();
        mediaContent.media = media;
        mediaContent.images = images;
        return mediaContent;
    }

    @SuppressWarnings({"unchecked"})
    static List<Image> readImages(String imagesJsonInput) throws Exception {
        JSON.Array<Value> imageValues = (JSON.Array<Value>) JSON.parse(imagesJsonInput).getRoot();
        return readImages(imageValues);
    }

    @SuppressWarnings("rawtypes")
    static List<Image> readImages(JSON.Array<Value> imageValues) throws Exception {
        int size = imageValues.size();
        List<Image> images = new ArrayList<Image>(size);
        for (int i = 0; i < size; i++) {
            JSON.Object imageJsonObject = (JSON.Object) imageValues.get(i);
            Image image = readImage(imageJsonObject);
            images.add(image);
        }
        return images;
    }

    @SuppressWarnings("rawtypes")
    static Image readImage(String imageJsonInput) throws Exception {
        JSON.Object imageJsonObject = (JSON.Object) JSON.parse(imageJsonInput).getRoot();
        return readImage(imageJsonObject);
    }

    @SuppressWarnings("rawtypes")
    static Image readImage(JSON.Object imageJsonObject) throws Exception {
        Image image = new Image();
        image.height = imageJsonObject.get("height").getInt();
        image.size = Image.Size.valueOf(imageJsonObject.get("size").getString());
        image.title = imageJsonObject.get("title").getString();
        image.uri = imageJsonObject.get("uri").getString();
        image.width = imageJsonObject.get("width").getInt();
        return image;
    }

    @SuppressWarnings({"rawtypes"})
    static Media readMedia(String mediaJsonInput) throws Exception {
        JSON.Object mediaJsonObject = (JSON.Object) JSON.parse(mediaJsonInput).getRoot();
        return readMedia(mediaJsonObject);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    static Media readMedia(JSON.Object mediaJsonObject) {
        Media media = new Media();
        Value bitrate = mediaJsonObject.get("bitrate");
        if (bitrate != null && !bitrate.isNull()) {
            media.bitrate = bitrate.getInt();
            media.hasBitrate = true;
        }
        Value copyright = mediaJsonObject.get("copyright");
        if (copyright != null && !copyright.isNull()) {
            media.copyright = copyright.getString();
        }
        media.duration = mediaJsonObject.get("duration").getLong();
        media.format = mediaJsonObject.get("format").getString();
        media.height = mediaJsonObject.get("height").getInt();
        JSON.Array<Value> personValues = (JSON.Array<Value>) mediaJsonObject.get("persons");
        int size = personValues.size();
        List<String> persons = new ArrayList<String>(size);
        for (int i = 0; i < size; i++) {
            persons.add(personValues.get(i).getString());
        }
        media.persons = persons;
        media.player = Media.Player.valueOf(mediaJsonObject.get("player").getString());
        media.size = mediaJsonObject.get("size").getLong();
        media.title = mediaJsonObject.get("title").getString();
        media.uri = mediaJsonObject.get("uri").getString();
        media.width = mediaJsonObject.get("width").getInt();
        return media;
    }

    static void writeMedia(StringWriter writer, Media media) throws Exception {
        JSON json = JSONMarshaler.marshalObject(media);
        writer.write(json.toJSON());
    }

    static void writeImage(StringWriter writer, Image image) throws Exception {
        JSON json = JSONMarshaler.marshalObject(image);
        writer.write(json.toJSON());
    }

    static void writeImages(StringWriter writer, List<Image> images) throws Exception {
        JSON json = JSONMarshaler.marshalObject(images);
        writer.write(json.toJSON());
    }

    static void writeMediaContent(StringWriter writer, MediaContent mediaContent) throws Exception {
        JSON json = JSONMarshaler.marshalObject(mediaContent);
        writer.write(json.toJSON());
    }
}