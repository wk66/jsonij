/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.jpath;

import cc.plural.jsonij.JPath;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;

/**
 * JPath Regex
 *
 * @author jmarsden@plural.cc
 */
public class JPathRegex {

    public JPathRegex() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNestedArrayRegex() throws ParserException, IOException {
        String jsonString = "{ \n"
                + "\"address_components\" : [\n"
                + "     {\n"
                + "          \"long_name\" : \"Hollywood\", \n"
                + "          \"short_name\" : \"Hollywood\",\n"
                + "          \"types\" : [ \"sublocality\", \"political\" ]\n"
                + "     },\n"
                + "     {\n"
                + "          \"long_name\" : \"Los Angeles\",\n"
                + "          \"short_name\" : \"Los Angeles\",\n"
                + "          \"types\" : [ \"locality\", \"political\" ]\n"
                + "      },\n"
                + "      {\n"
                + "           \"long_name\" : \"Los Angeles\",\n"
                + "           \"short_name\" : \"Los Angeles\",\n"
                + "           \"types\" : [ \"administrative_area_level_2\", \"political\" ]\n"
                + "       },\n"
                + "       {\n"
                + "           \"long_name\" : \"California\",\n"
                + "           \"short_name\" : \"CA\",\n"
                + "           \"types\" : [ \"administrative_area_level_1\", \"political\" ]\n"
                + "       },\n"
                + "       {\n"
                + "           \"long_name\" : \"United States\",\n"
                + "           \"short_name\" : \"US\",\n"
                + "           \"types\" : [ \"country\", \"political\" ]\n"
                + "       }\n"
                + "]}\n";
        JSON json = JSON.parse(jsonString);
        
        String regex1 = "\"^.*administrative_area_level_2.*$\"";
        System.out.println("Regex1:" + regex1);
        Value[] values = JPath.parse("/address_components[?(regex(@.types," + regex1 + "))]").evaluateAll(json);
        System.out.println("\tResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("\t[" + i + "]" + values[i].toJSON());
        }
        
        String regex2 = "\"^.*political.*$\"";
        System.out.println("Regex2:" + regex2);
        values = JPath.parse("/address_components[?(regex(@.types," + regex2 + "))]").evaluateAll(json);
        System.out.println("\tResultSize:" + values.length);
        for (int i = 0; i < values.length; i++) {
            System.out.println("\t[" + i + "]" + values[i].toJSON());
        }
    }
}
