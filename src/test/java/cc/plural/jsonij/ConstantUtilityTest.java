/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij;

import cc.plural.jsonij.ConstantUtility;
import junit.framework.TestCase;

/**
 * ConstantUtility Test Cases
 * 
 * @author jmarsden@plural.cc
 */
public class ConstantUtilityTest extends TestCase {

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link com.realitypipe.json.ConstantUtility#isDigitChar(char)}.
     */
    public void testIsDigitChar() {
        String testCaseString = "J0hn M45d3n\r";
        boolean[] testCaseResults = new boolean[]{false, true, false, false, false, false, true, true, false, true, false, false};
        int i = 0;
        for (char c : testCaseString.toCharArray()) {
            boolean expected = testCaseResults[i++];
            boolean result = ConstantUtility.isDigit(c);
            assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", String.valueOf(c), expected, result), expected, result);
        }
    }

    /**
     * Test method for {@link com.realitypipe.json.ConstantUtility#testIsHexDigitChar(char)}.
     */
    public void testIsHexDigitChar() {
        String testCaseString = "J0hf M45den\r";
        boolean[] testCaseResults = new boolean[]{false, true, false, true, false, false, true, true, true, true, false, false};
        int i = 0;
        for (char c : testCaseString.toCharArray()) {
            boolean expected = testCaseResults[i++];
            boolean result = ConstantUtility.isHexDigit(c);
            assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", String.valueOf(c), expected, result), expected, result);
        }
    }

    /**
     * Test method for {@link com.realitypipe.json.ConstantUtility#isWhiteSpace(int)}.
     */
    public void testIsWhiteSpaceInt() {
        String testCaseString = "W\rhi\te Sp\n";
        boolean[] testCaseResults = new boolean[]{false, false, false, false, true, false, true, false, false, false};
        int i = 0;
        for (char c : testCaseString.toCharArray()) {
            int r = (int) c;
            boolean expected = testCaseResults[i++];
            boolean result = ConstantUtility.isWhiteSpace(r);
            assertEquals(String.format("Incorrect Result for Int '%s' ('%s') Expected '%s' Result '%s'", String.valueOf(r), String.valueOf(c), expected, result), expected, result);
        }
    }

    /**
     * Test method for {@link com.realitypipe.json.ConstantUtility#isWhiteSpace(char)}.
     */
    public void testIsWhiteSpaceChar() {
        String testCaseString = "W\rhi\te Sp\n\t";
        boolean[] testCaseResults = new boolean[]{false, false, false, false, true, false, true, false, false, false, true};
        int i = 0;
        for (char c : testCaseString.toCharArray()) {
            boolean expected = testCaseResults[i++];
            boolean result = ConstantUtility.isWhiteSpace(c);
            assertEquals(String.format("Incorrect Result for Char '%s' Expected '%s' Result '%s'", Integer.toHexString(c), expected, result), expected, result);
        }
    }
}
