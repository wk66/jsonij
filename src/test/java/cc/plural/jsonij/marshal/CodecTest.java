/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.codec.DateJSONValueCodec;
import cc.plural.jsonij.marshal.codec.JSONValueCodecStore;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class CodecTest {

    public CodecTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        JSONValueCodecStore.clearCodecs();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCodecStoreRegister() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testCodecStoreRegister");

        JSONValueCodecStore codecStore = new JSONValueCodecStore();

        assertFalse(codecStore.hasCodec(Date.class));

        codecStore.registerCodec(Date.class, DateJSONValueCodec.class);

        assertTrue(codecStore.hasCodec(Date.class));
    }

    @Test
    public void testCodecStoreRegisterParent() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testCodecStoreRegisterParent");

        JSONValueCodecStore codecStore = new JSONValueCodecStore();

        assertFalse(codecStore.hasCodec(Date.class));

        codecStore.registerCodec(Date.class, DateJSONValueCodec.class);

        assertTrue(codecStore.hasCodec(InternalDate.class));
    }

    @Test
    public void testCodecStoreRegisterDate() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testCodecStoreRegisterDate");

        assertFalse(JSONMarshaler.hasCodec(Date.class));

        Date testDate = new Date();
        Value valueNoCodec = JSONMarshaler.marshalObjectToValue(testDate);

        System.out.println("Value No Codec:" + valueNoCodec);

        assertFalse(JSONMarshaler.hasCodec(Date.class));

        JSONMarshaler.registerCodec(Date.class, DateJSONValueCodec.class);

        assertTrue(JSONMarshaler.hasCodec(Date.class));

        Value valueCodec = JSONMarshaler.marshalObjectToValue(testDate);

        System.out.println("Value Codec:" + valueCodec);

///        assertEquals(valueCodec.toString(), "{\"time\":" + testDate.getTime() + "}");
    }

    @Test
    public void testCodecStoreRegisterDateEncodeDecode() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("testCodecStoreRegisterDateEncodeDecode");

        assertFalse(JSONMarshaler.hasCodec(Date.class));

        InternalDate testDate = new InternalDate();
        Value valueNoCodec = JSONMarshaler.marshalObjectToValue(testDate);

        System.out.println("Value No Codec:" + valueNoCodec);

        assertFalse(JSONMarshaler.hasCodec(InternalDate.class));

        JSONMarshaler.registerCodec(InternalDate.class, DateJSONValueCodec.class);

        assertTrue(JSONMarshaler.hasCodec(InternalDate.class));

        Value valueCodec = JSONMarshaler.marshalObjectToValue(testDate);

        System.out.println("Value Codec:" + valueCodec);

        InternalDate decodeDate = DateJSONValueCodec.decode(valueCodec, InternalDate.class);
        
        System.out.println("Decode Codec:" + decodeDate);
        
        //assertEquals(valueCodec.toString(), "{\"time\":" + testDate.getTime() + "}");
    }

    @Test
    public void testDateJSONValueCodec() throws JSONMarshalerException, IOException, ParserException, InstantiationException, IllegalAccessException {
        System.out.println("testCodecStoreRegisterDateChildEncodeDecode");

        InternalDate encodeDate = new InternalDate();

        System.out.println("EncodeDate:" + encodeDate);

        Value value = DateJSONValueCodec.encode(encodeDate);

        InternalDate decodeDate = DateJSONValueCodec.decode(value, InternalDate.class);

        System.out.println("DecodeDate:" + decodeDate);

        assertEquals(encodeDate.toString(), decodeDate.toString());
    }

    public static class InternalDate extends Date {
    }
}
