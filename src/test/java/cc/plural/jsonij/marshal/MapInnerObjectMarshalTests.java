/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import java.io.IOException;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.marshal.helpers.ListTypeNoProps;
import cc.plural.jsonij.marshal.helpers.MapTypeNoProps;
import cc.plural.jsonij.marshal.helpers.MapTypeProps;
import cc.plural.jsonij.parser.ParserException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
/**
 *
 * @author jmarsden
 */
public class MapInnerObjectMarshalTests {

    public MapInnerObjectMarshalTests() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMapNoPropsPrint() throws ParserException, IOException {
        System.out.println("testMapNoPropsPrint");
        MapTypeNoProps<String, String> map1 = new MapTypeNoProps<String, String>();
        map1.put("key1", "value1");
        map1.put("key2", "value2");

        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = false;
        JSON json1 = JSONMarshaler.marshalObject(map1);
        JSON jsonExpected1 = JSON.parse("{\"key2\":\"value2\",\"key1\":\"value1\"}");
        
        System.out.println("Parsed:" + json1.toJSON());
        System.out.println("Expected:" + jsonExpected1.toJSON());
        assertTrue(json1.equals(jsonExpected1));

        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = true;
        JSON json2 = JSONMarshaler.marshalObject(map1);
        
        JSON jsonExpected2 = JSON.parse("{\"$innerObject\":{\"key2\":\"value2\",\"key1\":\"value1\"}}");
        System.out.println("Parsed:" + json2.toJSON());
        System.out.println("Expected:" + jsonExpected2.toJSON());
        assertTrue(json2.equals(jsonExpected2));
    }

    @Test
    public void testMapPropsPrint() throws ParserException, IOException {
        System.out.println("testMapPropsPrint");
        MapTypeProps<String, String> map1 = new MapTypeProps<String, String>();
        map1.put("key1", "value1");
        map1.put("key2", "value2");
        map1.setPrivateInt(55);
        map1.publicString = "rah";

        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = false;
        JSON json1 = JSONMarshaler.marshalObject(map1);
        System.out.println(json1.toJSON());

        assertTrue(json1.equals(JSON.parse("{\"$innerObject\":{\"key2\":\"value2\",\"key1\":\"value1\"},\"privateInt\":55,\"publicString\":\"rah\"}")));
        
        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = true;
        JSON json2 = JSONMarshaler.marshalObject(map1);
        System.out.println(json2.toJSON());
        
        assertTrue(json2.equals(JSON.parse("{\"$innerObject\":{\"key2\":\"value2\",\"key1\":\"value1\"},\"privateInt\":55,\"publicString\":\"rah\"}")));
    }
    
    @Test
    public void testListNoPropsPrint() throws ParserException, IOException {
        System.out.println("testListNoPropsPrint");
        ListTypeNoProps<String> list = new ListTypeNoProps<String>();
        list.add("value1");
        list.add("value2");
        list.add("value3");
        
        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = false;
        JSON json1 = JSONMarshaler.marshalObject(list);
        System.out.println(json1.toJSON());

        assertTrue(json1.equals(JSON.parse("[\"value1\",\"value2\",\"value3\"]")));
        
        JSONMarshaler.ALWAYS_USE_INNER_PROPERTY = true;
        JSON json2 = JSONMarshaler.marshalObject(list);
        System.out.println(json2.toJSON());
        
        assertTrue(json2.equals(JSON.parse("{\"$innerArray\":[\"value1\",\"value2\",\"value3\"]}")));
    }
}
