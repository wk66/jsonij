/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.jsonij.marshal.helpers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author jmarsden
 */
public class HandlerConfiguration {

    Map<String, Class<?>> handlers;

    public HandlerConfiguration() {
        handlers = new HashMap<String, Class<?>>();
    }

    public Map<String, Class<?>> getHandlers() {
        return handlers;
    }

    public void setHandlers(Map<String, Class<?>> handlers) {
        this.handlers = handlers;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        Iterator<String> handlerIterator = handlers.keySet().iterator();
        while (handlerIterator.hasNext()) {
            String key = handlerIterator.next();
            Class<?> clazz = handlers.get(key);
            hash = 89 * hash + (key != null ? key.hashCode() : 0);
            hash = 7 * hash + (clazz != null ? clazz.hashCode() : 0);
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HandlerConfiguration other = (HandlerConfiguration) obj;
        if (other.handlers == null && handlers == null) {
            return true;
        } else if (other.handlers != null && handlers != null && other.handlers.size() == handlers.size()) {
            Iterator<String> handlerIterator = handlers.keySet().iterator();
            while (handlerIterator.hasNext()) {
                String key = handlerIterator.next();
                Class<?> class1 = handlers.get(key);
                Class<?> class2 = other.handlers.get(key);

                if (class1 == null && class2 == null) {
                    continue;
                } else if (class1.equals(class2)) {
                    continue;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "HandlerConfiguration{" + "handlers=" + handlers + '}';
    }
    
    
}