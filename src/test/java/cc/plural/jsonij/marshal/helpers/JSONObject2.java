package cc.plural.jsonij.marshal.helpers;

import java.lang.reflect.Array;

public class JSONObject2 {
    public String someValue;
    public int[] rah;
    
    public JSONObject2() {
        someValue = null;
    }
    
    @Override
    public String toString() {
        return String.format("JSONObject2[%s, %s]", someValue, (rah == null) ? "null" : arrayToString(rah));
    }
        
    public String arrayToString(Object array) {
        StringBuilder arrayStringBuilder = new StringBuilder();
        arrayStringBuilder.append('[');
        if(Array.getLength(array) > 0) {
            for(int i=0;i<Array.getLength(array)-1;i++) {
                arrayStringBuilder.append(Array.get(array, i)).append(',');
            }
            arrayStringBuilder.append(Array.get(array, Array.getLength(array)-1));
        }
        arrayStringBuilder.append(']');
 
        return arrayStringBuilder.toString();
    }
}
