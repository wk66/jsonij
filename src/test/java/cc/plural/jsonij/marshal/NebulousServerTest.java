/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.annotation.JSONCollector;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class NebulousServerTest {

    @Test
    public void testMapDetection() throws JSONMarshalerException, IOException, ParserException {

    }

    @Test
    public void testCollectorDetection() throws JSONMarshalerException, IOException, ParserException {

    }

    @Test
    public void testMarshalConfiguration() throws JSONMarshalerException, IOException, ParserException {
        URL url = getClass().getResource("/etc/config.json");
        InputStream stream = url.openStream();

        Object object = JSONMarshaler.marshalJSON(stream, NebulousConfig.class);

        JSON output = JSONMarshaler.marshalObject(object);

        System.out.println(output.toJSON());
    }

    public static class NebulousConfig {

        @JSONCollector(true)
        public HashMap<String, Value> values;

        public NebulousConfig() {
            values = new HashMap<String, Value>();
        }
    }

    public static class HttpConfig {

        String handler;

        String[] index;

        String mime;

        String defaultMime;

        ServerConfig server[];

        public HttpConfig() {
            handler = null;
        }

        public String getHandler() {
            return handler;
        }

        public void setHandler(String handler) {
            this.handler = handler;
        }

        public String[] getIndex() {
            return index;
        }

        public void setIndex(String[] index) {
            this.index = index;
        }

        public String getDefaultMime() {
            return defaultMime;
        }

        public void setDefaultMime(String defaultMime) {
            this.defaultMime = defaultMime;
        }

        public String getMime() {
            return mime;
        }

        public void setMime(String mime) {
            this.mime = mime;
        }

        public ServerConfig[] getServer() {
            return server;
        }

        public void setServer(ServerConfig[] server) {
            this.server = server;
        }
    }

    public static class ServerConfig {

        public int[] port;

        public LocationConfig[] location;

        public ServerConfig() {
            port = null;
            location = null;
        }
    }

    public static class LocationConfig {

        public String host;

        public String path;

        public RendererConfig renderer;

        public String accessLog;

        public String errorLog;

    }

    public static class RendererConfig {

        public String handler;

        public String jar;

    }
}
