 /*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.marshal.annotation.JSONName;
import cc.plural.jsonij.marshal.codec.ClassJSONValueCodec;
import cc.plural.jsonij.marshal.helpers.HandlerConfiguration;
import cc.plural.jsonij.marshal.helpers.JSONObject;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author jmarsden
 */
public class JSONDocumentMarshalerTest {

    @Test
    public void testMarshalJSONStringIntoIntArray() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("marshal JSON into Array");
        String inputJSONDocument = "[1,2,3,4,5]";
        JSON inputJSON = JSON.parse(inputJSONDocument);
        Object marshal = JSONMarshaler.marshalJSON(inputJSON, int[].class);
        JSON outputJSON = JSONMarshaler.marshalObject(marshal);
        System.out.println(String.format("InputJSON: %s", inputJSON));
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        System.out.println(String.format("OutputJSON: %s", outputJSON));
        assertNotNull(marshal);
        assertEquals(inputJSON, outputJSON);
    }

    @Test
    public void testMarshalJSONStringIntoObject() throws JSONMarshalerException, IOException, ParserException {
        System.out.println("marshal JSON into Object");
        String inputJSONDocument = "{\"message\":\"Hello JSON!\", \"rah\":-69, \"id\": 585757346726,    \"flag\": true, \"innerDoubleArray\": [1], \"inner\": {\"someValue\": \"hmmmn...\", \"rah\": [5,4,3,2,1]}}";

        JSON inputJSON = JSON.parse(inputJSONDocument);
        Object marshal = JSONMarshaler.marshalJSON(inputJSON, JSONObject.class);
        JSON outputJSON = JSONMarshaler.marshalObject(marshal);
        System.out.println(String.format("InputJSON: %s", inputJSON));
        System.out.println(String.format("MarshaledObjectToString: %s", marshal));
        System.out.println(String.format("OutputJSON: %s", outputJSON));

        assertNotNull(marshal);
        assertEquals(inputJSON, outputJSON);
    }

    @Test
    public void testMarshalHandlerClass() throws JSONMarshalerException, IOException, ParserException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        System.out.println("testMarshalHandlerClass");
        HandlerConfiguration config = new HandlerConfiguration();
        Map<String, Class<?>> handlers = new HashMap<String, Class<?>>();
        handlers.put("http", JSON.class);
        handlers.put("ftp", JSONMarshaler.class);
        config.setHandlers(handlers);
        
        JSON outputJSON = JSONMarshaler.marshalObject(config);
        System.out.println("Output JSON (no codec):" + outputJSON);

        JSONMarshaler.registerCodec(Class.class, ClassJSONValueCodec.class);

        outputJSON = JSONMarshaler.marshalObject(config);
        System.out.println("Output JSON (codec):" + outputJSON);

        HandlerConfiguration reload = (HandlerConfiguration) JSONMarshaler.marshalJSON(outputJSON, config.getClass());
        assertNotNull(reload);
        assertEquals(config.getHandlers(), reload.getHandlers());
    }
}
