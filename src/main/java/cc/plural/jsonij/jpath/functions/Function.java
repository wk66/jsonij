/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.jpath.functions;

import java.util.List;

import cc.plural.jsonij.Value;

/**
 *
 * @author jmarsden
 */
public abstract class Function {
    
    String functionName;
    
    List<Class<? extends FunctionArgument>> argumentTypeList;
    
    public Function() {
        functionName = null;
        functionName = null;
    }
    
    public String functionName() {
        return functionName;
    }
    
    public List<Class<? extends FunctionArgument>> getArgumentTypes() {
        return argumentTypeList;
    }
    
    public abstract Value evaluate(FunctionArgument[] args, Value value);
    
}
