/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.jpath;

import java.util.List;

import cc.plural.jsonij.Value;

public class StartEndStepPredicate extends PredicateComponent {

    int start;
    int end;
    int step;

    public StartEndStepPredicate() {
        this.start = -1;
        this.end = -1;
        this.step = -1;
    }

    public StartEndStepPredicate(int start, int end, int step) {
        this.start = start;
        this.end = end;
        this.step = step;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    @Override
    public List<Value> evaluate(List<Value> values, List<Value> results) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
