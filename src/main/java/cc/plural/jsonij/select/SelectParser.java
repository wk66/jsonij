/*
 * Copyright 2013 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.select;

import cc.plural.jsonij.ConstantUtility;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSONParserException;
import cc.plural.jsonij.jpath.KeyComponent;
import cc.plural.jsonij.jpath.PredicateComponent;
import cc.plural.jsonij.jpath.SearchComponent;
import cc.plural.jsonij.parser.BaseReaderParser;
import cc.plural.jsonij.parser.ParserException;
import cc.plural.jsonij.parser.ReaderParser;
import java.io.IOException;

import static cc.plural.jsonij.select.SelectConstants.OBJECT;
import static cc.plural.jsonij.select.SelectConstants.ARRAY;
import static cc.plural.jsonij.select.SelectConstants.NUMBER;
import static cc.plural.jsonij.select.SelectConstants.STRING;
import static cc.plural.jsonij.select.SelectConstants.BOOLEAN;
import static cc.plural.jsonij.select.SelectConstants.NULL;

import static cc.plural.jsonij.select.SelectConstants.CLASS_KEY;
import static cc.plural.jsonij.select.SelectConstants.PSEUDO_KEY;
import static cc.plural.jsonij.select.SelectConstants.UNIVERSAL_KEY;

/**
 *
 * @author jmarsden
 */
public class SelectParser {

    private StringBuilder selectorBuilder = null;

    public void parse(String select) throws IOException, ParserException {
        SelectParser.SelectReader target = new SelectParser.SelectReader(select);
        //return parse(target);
    }

    public void parse(SelectParser.SelectReader target) throws IOException, ParserException {

        while (target.peek() != -1) {
            Selector selector = null;
            if (SelectConstantUtility.isStartOfType(target.peek())) {
                selector = parseType(target);
            } else if (target.peek() == UNIVERSAL_KEY) {
                selector = parseUniversal(target);
            } else if (target.peek() == CLASS_KEY) {
                selector = parseClass(target);
            } else if (target.peek() == PSEUDO_KEY) {
                selector = parsePseudo(target);
            }

        }
    }

    private Selector parseType(SelectReader target) throws IOException, ParserException {
        int c = target.peek();
        Selector selector;
        
        if (c == 'o') {
            selector = parseTypeObject(target);
        } else if (c == 'a') {
            selector = parseTypeArray(target);
        } else if (c == 'n') {
            selector = parseTypeNumberOrNull(target);
        } else if (c == 's') {
            selector = parseTypeString(target);
        } else if (c == 'b') {
            selector = parseTypeBoolean(target);
        } else {
            throw new SelectParserException("typeerror", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }
        
        return selector;
    }

    private Selector parseTypeObject(SelectReader target) throws IOException, ParserException {
        for (int i = 0; i < OBJECT.length(); i++) {
            if (target.peek() == OBJECT.charAt(i)) {
                target.read();
            } else {
                // ERROR
            }
        }
        return Selector.OBJECT_SELECTOR;
    }

    private Selector parseTypeArray(SelectReader target) throws IOException, ParserException {
        for (int i = 0; i < ARRAY.length(); i++) {
            if (target.peek() == ARRAY.charAt(i)) {
                target.read();
            } else {
                // ERROR
            }
        }
        return Selector.ARRAY_SELECTOR;
    }

    private Selector parseTypeNumberOrNull(SelectReader target) throws IOException, ParserException {
        if (target.peek() == 'n') {
            target.read();
        } else {
            // ERROR
        }
        if (target.peek() == 'u') {
            target.read();
        } else {
            // ERROR
        }
        if (target.peek() == 'm') {
            target.read();
            for (int i = 3; i < NUMBER.length(); i++) {
                if (target.peek() == NUMBER.charAt(i)) {
                    target.read();
                } else {
                    // ERROR
                }
            }
            return Selector.NUMBER_SELECTOR;
        } else if (target.peek() == 'l') {
            for (int i = 3; i < NULL.length(); i++) {
                if (target.peek() == NULL.charAt(i)) {
                    target.read();
                } else {
                    // ERROR
                }
            }
            return Selector.NULL_SELECTOR;
        }
        return null;
    }

    private Selector parseTypeString(SelectReader target) throws IOException, ParserException {
        for (int i = 0; i < OBJECT.length(); i++) {
            if (target.peek() == OBJECT.charAt(i)) {
                target.read();
            } else {
                // ERROR
            }
        }
        return Selector.OBJECT_SELECTOR;
    }

    private Selector parseTypeBoolean(SelectReader target) throws IOException, ParserException {
        for (int i = 0; i < OBJECT.length(); i++) {
            if (target.peek() == OBJECT.charAt(i)) {
                target.read();
            } else {
                // ERROR
            }
        }
        return Selector.OBJECT_SELECTOR;
    }

    private Selector parseUniversal(SelectReader target) throws IOException, ParserException {
        return null;
    }

    private Selector parseClass(SelectReader target) throws IOException, ParserException {
        // Read Class Selector
        target.read();
        selectorBuilder = new StringBuilder();
        while (!ConstantUtility.isWhiteSpace(target.peek())) {
            selectorBuilder.append(target.read());
        }
        return new ClassSelector(selectorBuilder.toString());
    }

    private Selector parsePseudo(SelectReader target) throws IOException, ParserException {
        return null;
    }

    public static class SelectReader extends BaseReaderParser implements ReaderParser {

        String selectString;
        int index;

        public SelectReader(String selectString) {
            this.selectString = selectString;
            index = 0;
        }

        public String getSelect() {
            return selectString;
        }

        @Override
        public int readNext() throws ParserException {
            if (index < selectString.length()) {
                return selectString.charAt(index++);
            } else {
                return -1;
            }

        }

        public void skipWhitepace() throws IOException, ParserException {
            while (ConstantUtility.isWhiteSpace(peek())) {
                read();
            }
        }

        public void skipWhitepace(StringBuilder appender) throws IOException, ParserException {
            while (ConstantUtility.isWhiteSpace(peek())) {
                appender.append((char) read());
            }
        }
    }
}
