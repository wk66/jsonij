/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.jsonij.reflect;

import java.util.List;

/**
 *
 * @author jmarsden@plural.cc
 */
public class Inspection {

    private Class<?> klass;
    private List<ClassProperty> properties;
    private boolean innerList;
    private boolean innerMap;
    private List<ClassProperty> collectors;

    public Inspection() {
        klass = null;
        properties = null;
        innerList = false;
        innerMap = false;
        collectors = null;
    }

    public boolean hasInnerList() {
        return innerList;
    }

    public boolean isInnerList() {
        return innerList;
    }

    public void setInnerList(boolean innerList) {
        this.innerList = innerList;
    }

    public boolean hasInnerMap() {
        return innerMap;
    }

    public boolean isInnerMap() {
        return innerMap;
    }

    public void setInnerMap(boolean innerMap) {
        this.innerMap = innerMap;
    }

    public boolean hasCollectors() {
        return collectors != null && !collectors.isEmpty();
    }

    public List<ClassProperty> getCollectors() {
        return collectors;
    }

    public void setCollectors(List<ClassProperty> collectors) {
        this.collectors = collectors;
    }

    public Class<?> getKlass() {
        return klass;
    }

    public void setKlass(Class<?> klass) {
        this.klass = klass;
    }

    public List<ClassProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<ClassProperty> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Inspection [" + klass + " List:" + innerList + " Map:" + innerMap + " Properties:" + properties.size() + "]";
    }

    public boolean hasProperty(String name) {
        for (ClassProperty property : properties) {
            if (property.getPropertyName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public ClassProperty getProperty(String name) {
        for (ClassProperty property : properties) {
            if (property.getPropertyName().equals(name)) {
                return property;
            }
        }
        return null;
    }
}
