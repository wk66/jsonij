/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.jsonij.reflect;

import cc.plural.jsonij.marshal.annotation.JSONCollector;
import cc.plural.jsonij.marshal.annotation.JSONName;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmarsden@plural.cc
 */
public class InspectionFactory {

    public InspectionFilter filter;
    public static final String IS_PREFIX;
    public static final String SET_PREFIX;
    public static final String GET_PREFIX;

    static {
        IS_PREFIX = "is";
        SET_PREFIX = "set";
        GET_PREFIX = "get";
    }

    public InspectionFactory() {
        filter = InspectionFilter.getDefaultFilters();
    }

    public Inspection inspect(Object object) {
        return inspect(object.getClass());
    }

    public Inspection inspect(Class<?> klass) {
        if (klass == null) {
            throw new NullPointerException();
        }
        List<ClassProperty> tempProperties = new ArrayList<ClassProperty>();

        Class<?> currentClass = klass;
        do {
            if (filter.isFiltered(currentClass)) {
                continue;
            }
            List<ClassProperty> currentClassProperties = inspectClassProperties(currentClass);
            for (ClassProperty currentClassProperty : currentClassProperties) {
                if (!tempProperties.contains(currentClassProperty)) {
                    tempProperties.add(currentClassProperty);
                } else {
                    // TODO: Some kind of merge.
                }
            }
        } while ((currentClass = currentClass.getSuperclass()) != null);

        List<ClassProperty> classProperties = new ArrayList<ClassProperty>();
        List<ClassProperty> collectorProperties = new ArrayList<ClassProperty>();
        for (ClassProperty property : tempProperties) {
            if (property.isValid() && !classProperties.contains(property)) {
                classProperties.add(property);
            }
            if(property.isValid() && property.isCollector() && !collectorProperties.contains(property)) {
                collectorProperties.add(property);
            }
        }

        Inspection inspection = new Inspection();
        inspection.setKlass(klass);
        inspection.setProperties(classProperties);
        inspection.setInnerList(isListType(klass));
        inspection.setInnerMap(isMapType(klass));
        inspection.setCollectors(collectorProperties);
        return inspection;
    }

    private List<ClassProperty> inspectClassProperties(Class<?> klass) {
        Map<String, ClassProperty> propertyMap = new HashMap<String, ClassProperty>();
        Field[] fields = klass.getDeclaredFields();
        for (Field field : fields) {
            ClassProperty property = new ClassProperty();
            property.setKlass(klass);
            String propertyName = field.getName();
            if (field.isAnnotationPresent(JSONName.class)) {
                JSONName jsonName = (JSONName) field.getAnnotation(JSONName.class);
                propertyName = jsonName.value();
            }
            if (field.isAnnotationPresent(JSONCollector.class)) {
                property.setCollector(true);
            }
            property.setPropertyName(propertyName);
            property.registerAccessor(field);
            property.registorMutator(field);
            propertyMap.put(propertyName, property);
        }
        Method[] methods = klass.getDeclaredMethods();
        for (Method method : methods) {
            char ch;
            String propertyName = null;
            String methodName = method.getName();
            boolean collector = false;
            
            Class<?> returnType = method.getReturnType();
            if (methodName.length() > IS_PREFIX.length() && methodName.startsWith(IS_PREFIX) && returnType == boolean.class && Character.isUpperCase(ch = methodName.charAt(IS_PREFIX.length()))) {
                if (method.isAnnotationPresent(JSONName.class)) {
                    JSONName jsonName = (JSONName) method.getAnnotation(JSONName.class);
                    propertyName = jsonName.value();
                } else {
                    propertyName = Character.toLowerCase(ch) + methodName.substring(IS_PREFIX.length() + 1, methodName.length());
                }
                if (method.isAnnotationPresent(JSONCollector.class)) {
                    collector = true;
                }
                ClassProperty property = null;
                if (propertyMap.containsKey(propertyName)) {
                    property = propertyMap.get(propertyName);
                    property.registerAccessor(method);
                } else {
                    property = new ClassProperty();
                    property.setKlass(klass);
                    property.setPropertyName(propertyName);
                    property.registerAccessor(method);
                    propertyMap.put(propertyName, property);
                    property.setCollector(collector);
                }
            } else if (methodName.length() > SET_PREFIX.length() && methodName.startsWith(SET_PREFIX) && Character.isUpperCase(ch = methodName.charAt(SET_PREFIX.length()))) {
                propertyName = Character.toLowerCase(ch) + methodName.substring(SET_PREFIX.length() + 1, methodName.length());
                ClassProperty property = null;
                if (propertyMap.containsKey(propertyName)) {
                    property = propertyMap.get(propertyName);
                    property.registorMutator(method);
                } else {
                    property = new ClassProperty();
                    property.setKlass(klass);
                    property.setPropertyName(propertyName);
                    property.registorMutator(method);
                    propertyMap.put(propertyName, property);
                }
            } else if (methodName.length() > GET_PREFIX.length() && methodName.startsWith(GET_PREFIX) && Character.isUpperCase(ch = methodName.charAt(GET_PREFIX.length())) && returnType != null) {
                propertyName = Character.toLowerCase(ch) + methodName.substring(GET_PREFIX.length() + 1, methodName.length());
                ClassProperty property = null;
                if (propertyMap.containsKey(propertyName)) {
                    property = propertyMap.get(propertyName);
                    property.registerAccessor(method);
                } else {
                    property = new ClassProperty();
                    property.setKlass(klass);
                    property.setPropertyName(propertyName);
                    property.registerAccessor(method);
                    propertyMap.put(propertyName, property);
                }
            }
        }
        List<ClassProperty> propertyList = new ArrayList<ClassProperty>();
        propertyList.addAll(propertyMap.values());
        return propertyList;
    }

    public static boolean isMapType(Class<?> c) {
        Class<?> currentClass = c;
        do {
            Class<?>[] interfaces = currentClass.getInterfaces();
            for (Class<?> i : interfaces) {
                if (i == Map.class) {
                    return true;
                }
            }
        } while ((currentClass = currentClass.getSuperclass()) != null);
        return false;
    }

    public static boolean isListType(Class<?> c) {
        Class<?> currentClass = c;
        do {
            Class<?>[] interfaces = currentClass.getInterfaces();
            for (Class<?> i : interfaces) {
                if (i == List.class) {
                    return true;
                }
            }
        } while ((currentClass = currentClass.getSuperclass()) != null);
        return false;
    }
}
