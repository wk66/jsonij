/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.jsonij.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;

/**
 *
 * @author jmarsden@plural.cc
 */
public class ClassProperty {

    private Class<?> klass;
    private String propertyName;
    private ClassPropertyAccessor accessor;
    private ClassPropertyMutator mutator;
    private boolean collector;
    
    public ClassProperty() {
        klass = null;
        propertyName = null;
        accessor = null;
        mutator = null;
        collector = false;
    }

    public ClassPropertyAccessor getAccessor() {
        return accessor;
    }

    public void setAccessor(ClassPropertyAccessor accessor) {
        this.accessor = accessor;
    }

    public Class<?> getKlass() {
        return klass;
    }

    public void setKlass(Class<?> klass) {
        this.klass = klass;
    }

    public ClassPropertyMutator getMutator() {
        return mutator;
    }

    public void setMutator(ClassPropertyMutator mutator) {
        this.mutator = mutator;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public boolean isCollector() {
        return collector;
    }

    public void setCollector(boolean collector) {
        this.collector = collector;
    }

    public void registerAccessor(Field field) {
        if (accessor == null) {
            accessor = new ClassPropertyAccessor();
            accessor.field = field;
            accessor.klass = field.getDeclaringClass();
            accessor.type = field.getType();
            accessor.genericType = field.getGenericType();
        } else if (accessor.field == null) {
            accessor.field = field;
        }
    }

    public void registorMutator(Field field) {
        if (mutator == null) {
            mutator = new ClassPropertyMutator();
            mutator.field = field;
            mutator.klass = field.getDeclaringClass();
            mutator.type = field.getType();
            mutator.genericType = field.getGenericType();
        } else if (mutator.field == null) {
            mutator.field = field;
        }
    }

    public void registerAccessor(Method method) {
        if (accessor == null) {
            accessor = new ClassPropertyAccessor();
            accessor.method = method;
            accessor.klass = method.getDeclaringClass();
            accessor.type = method.getReturnType();
            accessor.genericType = method.getGenericReturnType();
        } else if (accessor.method == null) {
            accessor.method = method;
        }
    }

    public void registorMutator(Method method) {
        if (mutator == null) {
            Class<?>[] argumentTypes = method.getParameterTypes();
            Type[] argumentGenericTypes = method.getGenericParameterTypes();
            if (argumentTypes.length == 1) {
                mutator = new ClassPropertyMutator();
                mutator.method = method;
                mutator.klass = method.getDeclaringClass();
                mutator.type = argumentTypes[0];
                mutator.genericType = argumentGenericTypes[0];
            }
        } else if (mutator.method == null) {
            mutator.method = method;
        }
    }

    public boolean isValid() {
        if (mutator != null && accessor != null && mutator.canAccess() && accessor.canAccess()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassProperty other = (ClassProperty) obj;
        if (!Objects.equals(this.propertyName, other.propertyName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.klass);
        hash = 67 * hash + Objects.hashCode(this.propertyName);
        return hash;
    }

    @Override
    public String toString() {
        return klass.getSimpleName() + "." + propertyName + "(" + mutator + "): " + accessor;
    }

    public static class ClassPropertyAccessor {

        private Class<?> klass;
        private Class<?> type;
        private Type genericType;
        private Method method;
        private Field field;

        public Field getField() {
            return field;
        }

        public void setField(Field field) {
            this.field = field;
        }

        public Class<?> getKlass() {
            return klass;
        }

        public void setKlass(Class<?> klass) {
            this.klass = klass;
        }

        public Method getMethod() {
            return method;
        }

        public void setMethod(Method method) {
            this.method = method;
        }

        public Class<?> getType() {
            return type;
        }

        public void setType(Class<?> type) {
            this.type = type;
        }

        public Type getGenericType() {
            return genericType;
        }

        public void setGenericType(Type genericType) {
            this.genericType = genericType;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            if (field != null) {
                int modifiers = field.getModifiers();
                builder.append(getModifierString(modifiers));
                builder.append('f');
            }

            if (method != null) {
                int modifiers = method.getModifiers();
                builder.append(getModifierString(modifiers));
                builder.append('m');
            }
            builder.append('[');
            builder.append(getTypeString(type));
            builder.append(']');
            return builder.toString();
        }

        public boolean canAccess() {
            if (field != null && Modifier.isPublic(field.getModifiers())) {
                return true;
            }
            if (method != null && Modifier.isPublic(method.getModifiers())) {
                return true;
            }
            return false;
        }

        public boolean fieldType() {
            if (field != null && Modifier.isPublic(field.getModifiers())) {
                return true;
            }
            return false;
        }

        public boolean methodType() {
            if (method != null && Modifier.isPublic(method.getModifiers())) {
                return true;
            }
            return false;
        }
    }

    public static class ClassPropertyMutator {

        private Class<?> klass;
        private Class<?> type;
        private Type genericType;
        private Method method;
        private Field field;

        public Field getField() {
            return field;
        }

        public void setField(Field field) {
            this.field = field;
        }

        public Class<?> getKlass() {
            return klass;
        }

        public void setKlass(Class<?> klass) {
            this.klass = klass;
        }

        public Method getMethod() {
            return method;
        }

        public void setMethod(Method method) {
            this.method = method;
        }

        public Class<?> getType() {
            return type;
        }

        public void setType(Class<?> type) {
            this.type = type;
        }

        public Type getGenericType() {
            return genericType;
        }

        public void setGenericType(Type genericType) {
            this.genericType = genericType;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            if (field != null) {
                int modifiers = field.getModifiers();
                builder.append(getModifierString(modifiers));
                builder.append('f');
            }

            if (method != null) {
                int modifiers = method.getModifiers();
                builder.append(getModifierString(modifiers));
                builder.append('m');
            }
            builder.append('[');
            builder.append(getTypeString(type));
            builder.append(']');
            return builder.toString();
        }

        public boolean canAccess() {
            if (field != null && Modifier.isPublic(field.getModifiers())) {
                return true;
            }
            if (method != null && Modifier.isPublic(method.getModifiers())) {
                return true;
            }
            return false;
        }

        public boolean fieldType() {
            if (field != null && Modifier.isPublic(field.getModifiers())) {
                return true;
            }
            return false;
        }

        public boolean methodType() {
            if (method != null && Modifier.isPublic(method.getModifiers())) {
                return true;
            }
            return false;
        }

        public void fire(Object object, boolean bool) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, bool);
            }
            if (fieldType()) {
                field.set(object, bool);
            }
        }

        public void fire(Object object, byte byteValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, byteValue);
            }
            if (fieldType()) {
                field.set(object, byteValue);
            }
        }

        public void fire(Object object, short shortValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, shortValue);
            }
            if (fieldType()) {
                field.set(object, shortValue);
            }
        }

        public void fire(Object object, float floatValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, floatValue);
            }
            if (fieldType()) {
                field.set(object, floatValue);
            }
        }

        public void fire(Object object, int intValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, intValue);
            }
            if (fieldType()) {
                field.set(object, intValue);
            }
        }

        public void fire(Object object, long longValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, longValue);
            }
            if (fieldType()) {
                field.set(object, longValue);
            }
        }

        public void fire(Object object, String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, string);
            }
            if (fieldType()) {
                field.set(object, string);
            }
        }

        public void fire(Object object, Object propertyObject) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (methodType()) {
                method.invoke(object, propertyObject);
            }
            if (fieldType()) {
                field.set(object, propertyObject);
            }
        }
    }

    public static String getModifierString(int modifiers) {
        String modifier = null;
        if (Modifier.isPublic(modifiers)) {
            modifier = "+";
        } else if (Modifier.isPrivate(modifiers)) {
            modifier = "-";
        } else if (Modifier.isProtected(modifiers)) {
            modifier = "#";
        } else {
            modifier = "%";
        }
        return modifier;
    }

    public static String getTypeString(Type genericType) {
        StringBuilder builder = new StringBuilder();
        if (genericType instanceof Class) {
            Class<?> genericTypeClass = (Class<?>) genericType;
            builder.append(genericTypeClass.getSimpleName());
        } else if (genericType instanceof ParameterizedType) {
            ParameterizedType genericParameterizedType = (ParameterizedType) genericType;
            builder.append(getTypeString(genericParameterizedType.getRawType()));
            Type[] innerTypes = genericParameterizedType.getActualTypeArguments();
            builder.append('<');
            if (innerTypes.length > 0) {
                for (int i = 0; i < innerTypes.length - 1; i++) {
                    builder.append(getTypeString(innerTypes[i])).append(',');
                }
                builder.append(getTypeString(innerTypes[innerTypes.length - 1]));
            }
            builder.append('>');
        }
        return builder.toString();
    }
}
