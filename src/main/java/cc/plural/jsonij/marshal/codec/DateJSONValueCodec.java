/*
 * Copyright 2012 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij.marshal.codec;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jmarsden
 */
public class DateJSONValueCodec implements JSONValueCodec {
    
    public static DateFormat ISO8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    
    public static <D extends java.util.Date> Value encode(D d) {
        return new JSON.String(ISO8601_FORMAT.format(d));
    }
    
    public static <D extends java.util.Date> D decode(Value value, Class<D> clazz) throws JSONMarshalerException {
        if(value.type() == Value.TYPE.STRING) {
            D decodedObject = null;
            try {
                decodedObject = clazz.newInstance();
                
                Date date = ISO8601_FORMAT.parse(value.getString());
                decodedObject.setTime(date.getTime());
            } catch (ParseException ex) {
                Logger.getLogger(DateJSONValueCodec.class.getName()).log(Level.SEVERE, null, ex);
                throw new JSONMarshalerException("decode");
            } catch (InstantiationException ex) {
                Logger.getLogger(DateJSONValueCodec.class.getName()).log(Level.SEVERE, null, ex);
                throw new JSONMarshalerException("decode");
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DateJSONValueCodec.class.getName()).log(Level.SEVERE, null, ex);
                throw new JSONMarshalerException("decode");
            }
            return decodedObject;
        } else {
            throw new JSONMarshalerException("notdate");
        }   
    }
}
