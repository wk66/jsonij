/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package cc.plural.jsonij.marshal;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.codec.JSONValueCodec;
import cc.plural.jsonij.marshal.codec.JSONValueCodecStore;
import cc.plural.jsonij.parser.ParserException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author jmarsden@plural.cc
 */
public class JSONMarshaler {

    protected static final JavaMarshaler JAVA_MARSHALLER;
    protected static final JSONDocumentMarshaler JSON_DOCUMENT_MARSHALER;
    public static boolean ALWAYS_SPECIFY_CLASS;
    public static String CLASS_PROPERTY;
    public static boolean ALWAYS_USE_INNER_PROPERTY;
    public static String INNER_ARRAY_PROPERTY;
    public static String INNER_OBJECT_PROPERTY;
    static JSONValueCodecStore CODEC_STORE;
    static int CYCLE_LEVELS;

    static {
        JAVA_MARSHALLER = new JavaMarshaler();
        JSON_DOCUMENT_MARSHALER = new JSONDocumentMarshaler();
        ALWAYS_SPECIFY_CLASS = false;
        CLASS_PROPERTY = "$class";
        ALWAYS_USE_INNER_PROPERTY = false;
        INNER_ARRAY_PROPERTY = "$innerArray";
        INNER_OBJECT_PROPERTY = "$innerObject";
        CODEC_STORE = null;
        CYCLE_LEVELS = 0;
    }

    public static JSON marshalObject(Object o) throws JSONMarshalerException {
        Value mashaledObject = JAVA_MARSHALLER.marshalObject(o);
        return new JSON(mashaledObject);
    }

    public static JSON marshalObject(boolean[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Boolean[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(int[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Integer[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(char[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Character[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(double[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Double[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(float[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Float[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(short[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Short[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(long[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Long[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(String[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Object[] a) throws JSONMarshalerException {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static Value marshalObjectToValue(Object o) throws JSONMarshalerException {
        Value marshaledObject = JAVA_MARSHALLER.marshalObject(o);
        return marshaledObject;
    }

    public static Value marshalObjectToValue(boolean[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Boolean[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(int[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Integer[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(char[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Character[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(double[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Double[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(float[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Float[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(short[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Short[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(long[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Long[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(String[] a) {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Value marshalObjectToValue(Object[] a) throws JSONMarshalerException {
        Value marshaledArray = JAVA_MARSHALLER.marshalObject(a);
        return marshaledArray;
    }

    public static Object marshalJSON(String json, Class<?> c) throws JSONMarshalerException, IOException, ParserException {
        Object marshaledObject = JSON_DOCUMENT_MARSHALER.marshalJSONDocument(JSON.parse(json), c);
        return marshaledObject;
    }

    public static Object marshalJSON(JSON json, Class<?> c) throws JSONMarshalerException {
        Object marshaledObject = JSON_DOCUMENT_MARSHALER.marshalJSONDocument(json, c);
        return marshaledObject;
    }

    public static Object marshalJSON(Value value, Class<?> c) throws JSONMarshalerException {
        Object marshaledObject = JSON_DOCUMENT_MARSHALER.marshalJSONDocument(value, c);
        return marshaledObject;
    }

    public static Object marshalJSON(InputStream stream, Class<?> c) throws JSONMarshalerException {
        Object marshaledObject = JSON_DOCUMENT_MARSHALER.marshalJSONDocument(stream, c);
        return marshaledObject;
    }

    public static void resetCodecStore() {
        if (CODEC_STORE != null) {
            CODEC_STORE = null;
        }
    }

    public static boolean hasCodec(Class<?> codecType) {
        if (CODEC_STORE == null) {
            return false;
        } else {
            return CODEC_STORE.hasCodec(codecType);
        }
    }

    public static void registerCodec(Class<?> type, Class<? extends JSONValueCodec> codec) {
        if (CODEC_STORE == null) {
            CODEC_STORE = new JSONValueCodecStore();
        }
        CODEC_STORE.registerCodec(type, codec);
    }

    public static JSONValueCodecStore.JSONValueCodecHelper getCodecHelper(Class<?> type) {
        if (CODEC_STORE == null) {
            return null;
        } else {
            return CODEC_STORE.getCodecHelper(type);
        }
    }

    public static Class<? extends JSONValueCodec> getCodec(Class<?> type) {
        if (CODEC_STORE == null) {
            return null;
        } else {
            return CODEC_STORE.getCodec(type);
        }
    }

    public static int getCycleLevels() {
        return CYCLE_LEVELS;
    }

    public static void setCycleLevels(int levels) {
        CYCLE_LEVELS = levels;
    }
}
