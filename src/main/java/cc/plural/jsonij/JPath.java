/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package cc.plural.jsonij;

import java.io.IOException;
import java.io.Reader;

import cc.plural.jsonij.jpath.Component;
import cc.plural.jsonij.jpath.JPathImp;
import cc.plural.jsonij.jpath.JPathParser;
import cc.plural.jsonij.parser.ParserException;

/**
 * JPath implementation. Inspired by XPath and <a
 * href="http://goessner.net/articles/JsonPath/">JsonPath</a>.
 *
 * @author J.W.Marsden
 */
public class JPath<C extends Component> extends JPathImp<C> {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = -1908871021763306556L;

    public static final JPathParser parser;
    
    static {
        parser = new JPathParser();
    }
    
    public JPath() {
    }

    public static JPath<Component> parse(String path) throws IOException, ParserException {
        JPath<Component> jPath = parser.parse(path);
        return jPath;
    }

    public static Value evaluate(java.lang.String document, java.lang.String jPath) throws ParserException, IOException {
        JSON jsonDocument = JSON.parse(document);
        JPath<?> jPathInstance = parser.parse(jPath);
        return jPathInstance.evaluate(jsonDocument);
    }

    public static Value evaluate(Reader documentReader, java.lang.String jPath) throws ParserException, IOException {
        JSON jsonDocument = JSON.parse(documentReader);
        JPath<?> jPathInstance = parser.parse(jPath);
        return jPathInstance.evaluate(jsonDocument);
    }

    public static Value evaluate(JSON jsonDocument, java.lang.String jPath) throws ParserException, IOException {
        JPath<?> jPathInstance = parser.parse(jPath);
        return jPathInstance.evaluate(jsonDocument);
    }

    public static Value evaluate(Value value, java.lang.String jPath) throws ParserException, IOException {
        JPath<?> jPathInstance = parser.parse(jPath);
        return jPathInstance.evaluate(value);
    }
}
